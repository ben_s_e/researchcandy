/*
 * ResearchCandy javascript
 *  - copy links to clipboard
 *  - showing paper commentaries
 *  - showing menus etc.
 *
 * Ben Shirt-Ediss
 */


var clock = null;
var clock2 = null;



/*
 * COPY TO CLIPBOARD
 *
 * The following code is credited to Dean Taylor's answer on Stack Exchange (2015):
 * https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
 *
 */

function cc(text) {

	var textArea = document.createElement("textarea");

  	//
  	// *** This styling is an extra step which is likely not required. ***
  	//
  	// Why is it here? To ensure:
  	// 1. the element is able to have focus and selection.
  	// 2. if element was to flash render it has minimal visual impact.
  	// 3. less flakyness with selection and copying which **might** occur if
  	//    the textarea element is not visible.
  	//
  	// The likelihood is the element won't even render, not even a flash,
  	// so some of these are just precautions. However in IE the element
  	// is visible whilst the popup box asking the user for permission for
  	// the web page to copy to the clipboard.
  	//

  	// Place in top-left corner of screen regardless of scroll position.
  	textArea.style.position = 'fixed';
  	textArea.style.top = 0;
  	textArea.style.left = 0;

  	// Ensure it has a small width and height. Setting to 1px / 1em
  	// doesn't work as this gives a negative w/h on some browsers.
  	textArea.style.width = '2em';
  	textArea.style.height = '2em';

  	// We don't need padding, reducing the size if it does flash render.
  	textArea.style.padding = 0;

  	// Clean up any borders.
  	textArea.style.border = 'none';
  	textArea.style.outline = 'none';
  	textArea.style.boxShadow = 'none';

  	// Avoid flash of white box if rendered for any reason.
  	textArea.style.background = 'transparent';

  	textArea.value = text;

  	document.body.appendChild(textArea);

  	textArea.select();

	try {
		var successful = document.execCommand('copy');
		var msg = successful ? 'successful' : 'unsuccessful';
		console.log('Copying text command was ' + msg);

		if (successful) {
			cc_indicator_on()
		}
  	} catch (err) {
		console.log('Oops, unable to copy');
  	}

  	document.body.removeChild(textArea);
}



// turn 'copied to clipboard' indicator on/off temporarily
function cc_indicator_on() {

  document.getElementById("toclipboard").style.display = "block";
	if (clock != null) {
		window.clearInterval(clock);
	}
	clock = setInterval("cc_indicator_off()", 600);
}


function cc_indicator_off() {
	document.getElementById("toclipboard").style.display = "none";
	window.clearInterval(clock);
	clock = null;
}





/*
 * TOP RE-INDEX MENU SHOW/HIDE
 *
 */

function show_reindex_menu() {
	document.getElementById("reindex_menu").style.display = "block";
}


function hide_reindex_menu() {
	document.getElementById("reindex_menu").style.display = "none";
}





/*
 * PAPER CLICK
 *    (copy link to clipboard and show commentary box)
 */

function pc(pid) {
  // left paper click: copy link to clipboard

  var topic = document.getElementById("t" + pid).innerHTML;
  var subtopic = "";
  if (document.getElementById("s" + pid)) {
    subtopic = document.getElementById("s" + pid).innerHTML + "/";
  }  
	var title = document.getElementById("n" + pid).innerHTML;
	

  // copy link to clipboard
  // (decode any special HTML entities in the link, back to their original chars (like &amp; for &)
  pp = collection_path + topic + "/" + subtopic + title + ".pdf"
  var txt = document.createElement('textarea');
  txt.innerHTML = pp;
	cc(txt.value)    // https://gomakethings.com/decoding-html-entities-with-vanilla-javascript/
}



function rpc(pid) {
  // right paper click: open commentary box if its closed, close it if its open
  // https://stackoverflow.com/questions/4235426/how-can-i-capture-the-right-click-event-in-javascript

  if (document.getElementById("e" + pid)) {
    if (document.getElementById("e" + pid).style.display == "block") {
      document.getElementById("e" + pid).style.display = "none";  
    }
    else {
      document.getElementById("e" + pid).style.display = "block"; 
    }
  }
}




/*
 * CLOSE COMMENTARY BOX
 *    (When clicking the "Commentary" link at the top of commentary box)
 */
function clc(pid) {
  if (document.getElementById("e" + pid)) {
    document.getElementById("e" + pid).style.display = "none";
  }
}




/*
 * OPEN COMMENTARY BOX FOR PAPER INDICATED BY #p ANCHOR IN URL
 *    (fired by page onload handler)
 *     
 */

function zoom_to_url_anchor() {

  if (window.location.hash.length > 2) {
    // hash is #p<paperid>
    var pid = window.location.hash.replace("#p", "")

    // open paper commentary box, if it exists
    if (document.getElementById("e" + pid)) {
      document.getElementById("e" + pid).style.display = "block";
    }
    else {
      // no commentary box -- highlight paper temporarily, to show it was updated
      highlight_paper_on(pid)
    }
  }
}


function highlight_paper_on(pid) {

  document.getElementById("pp" + pid).style.backgroundColor = "#ced6e1";
  clock2 = setInterval("highlight_paper_off(" + pid + ")", 1200);
}


function highlight_paper_off(pid) {
  
  document.getElementById("pp" + pid).style.backgroundColor = "";
  window.clearInterval(clock2);
  clock2 = null;
}





/*
 * STAR RATING ON PAPER DETAILS PAGE
 *
 */

function update_star_display() {

  switch(document.getElementById("star_rating").selectedIndex) {
    case 0:
        document.getElementById("star_display").innerHTML = '<span class="s0">&#9734;&#9734;&#9734;</span>'
        break;
    case 1:
        document.getElementById("star_display").innerHTML = '<span class="s1">&#9733;</span><span class="s0">&#9734;&#9734;</span>'
        break;
    case 2:
        document.getElementById("star_display").innerHTML = '<span class="s2">&#9733;&#9733;</span><span class="s0">&#9734;</span>'
        break;
    case 3:
        document.getElementById("star_display").innerHTML = '<span class="s3">&#9733;&#9733;&#9733;</span>'
        break;
  }   
}


/*
 * EXTERNAL FILE ON PAPER DETAILS PAGE
 *    
 */

function add_external_file_md_example() {

  ef = document.getElementById("extra_files").value
  document.getElementById("extra_files").value = ef + "[Link name](/absolute/path/to/file)" + "\n"
}


