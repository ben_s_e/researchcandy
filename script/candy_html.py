#!/usr/bin/env python3

"""HTML5 and Javascript output to build ResearchCandy index page
	(the index page is not a Flask template, its a static page entirely built here)

Ben Shirt-Ediss, 2018-2019
"""

import candy_misc
import mistune		# fast markdown to HTML converter (https://github.com/lepture/mistune)
import html 		# to escape HTML chars


#
#
# Main index page HTML
#
#

def headerHTML(collection_name, pdf_path) :

	# note that the css and javascript need to be in /static
	# in order for the flask web app to work

	out = """<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">

	<title>%s</title>

	<link rel="stylesheet" href="/static/candy.css">
	<script src="/static/candy.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		var collection_path = "%s/";
	</script>
</head>
<body onload="zoom_to_url_anchor()">
""" % (collection_name, pdf_path)

	return out
	




def md2html(text) :
	"""
	Convert user input markdown, into HTML, using mistune
	"""

	# first, detect hash tags in the markdown, and convert them to HTML spans
	htags = candy_misc.parse_hashtags(text)

	#now replace hash tags with HTML markup
	for h in htags :
		text = text.replace(h, "<span class=\"ht\">&#35;%s</span>" % (h[1:]))  # escape char used for hash so not interpreted as markdown

	# second, convert remaining markdown to HTML
	# and don't escape HTML in the markdown, so it will be interpreted by browser
	return mistune.markdown(text, escape=False)							





def headerRC(version_str, pdf_path, show_commentaries, num_papers, num_topics, collection_name) :

	show_commentaries_checkbox = ""  	# always reverts to DON'T show all commentaries
	#if show_commentaries :
	#	show_commentaries_checkbox = "checked"

	out = """
	<div id="reindex_menu">
		<div id="version">
			%s
		</div>
		<div id="path">
			PDF collection location: <span class="code">%s</span>
		</div>
		<div id="expand_commentaries">
			<form id="rebuild_form" method="POST" action="/">
				<input type="hidden" name="rebuild" value="yes"/>
				<input type="checkbox" name="show_commentaries" value="yes" %s/> Show ALL commentaries unfolded, after re-indexing collection
			</form>
		</div>
		<div id="buttons">
			<a onclick="hide_reindex_menu()" href="javascript:void(0);" class="cancel_button">Cancel</a>
			<a onclick="document.getElementById('rebuild_form').submit()" href="javascript:void(0);" class="confirm_button"><span class="ri_symbol">&#8635;</span> Re-Index PDF Collection</a>
		</div>
	</div>
	<div id="toclipboard">
		&#9712; Link copied to clipboard
	</div>	
	<div id="header">
		<div id="collection">
			%s
			<div id="collection_info">
				<a onclick="show_reindex_menu()" href="javascript:void(0);">&#9776;</a> <strong>%s</strong> papers &#9883; <strong>%s</strong> topics &#9883; <span id="num_commentaries"><strong>0</strong> commentaries</span>
			</div>
		</div>
	</div>
	<div id="articles">
	""" % (version_str, pdf_path, show_commentaries_checkbox, collection_name, num_papers, num_topics)

	return out




def paper(paper_id, topic, subtopic, papername, text_colour, topic_colour, paperinfo, show_commentaries) :

	# add HTML escape chars to paper name
	topic = html.escape(topic)
	subtopic = html.escape(subtopic)
	papername = html.escape(papername)

	# stars
	stars_num = paperinfo["stars"]

	stars_html = "<span class=\"s0\">&#9734;&#9734;&#9734;</span>"
	if stars_num == 1 :
		stars_html = "<span class=\"s1\">&#9733;</span><span class=\"s0\">&#9734;&#9734;</span>"
	elif stars_num == 2 :
		stars_html = "<span class=\"s2\">&#9733;&#9733;</span><span class=\"s0\">&#9734;</span>"
	elif stars_num == 3 :
		stars_html = "<span class=\"s3\">&#9733;&#9733;&#9733;</span>"

	# subtopic
	subtopic_html = ""
	if subtopic != "" :
		subtopic_html = "<span class=\"subtopic\" style=\"border-color: %s\" id=\"s%d\">%s</span>" % (topic_colour, paper_id, subtopic)

	# extra paper details (commentary, image and links to external files)
	#
	# neccessary to display these are:
	#		novelty
	# 	OR	image
	# 	OR	link
	#	(or any combination of these. Commentary text may also be present)

	
	extra_html = ""
	if paperinfo["commentary_exists"] :

		novelty_html = ""
		if paperinfo["novelty_md"] != "" :
			novelty_html = 	"<div class=\"novelty\">%s</div>" % (md2html(paperinfo["novelty_md"]))

		image_html = ""
		if paperinfo["image"] != "" :
			# image lies across top
			if paperinfo["image_position"] == 1 :
				# image is centred
				image_html = "<div class=\"centre\"><a href=\"%s\" target=\"_blank\"><img class=\"cc\" width=\"%d%%\" src=\"%s\" /></a></div>" % (paperinfo["image"], paperinfo["image_width_percent"], paperinfo["image"])				
			else :
				# image is at right hand side
				image_html = "<a href=\"%s\"><img class=\"cr\" width=\"%d%%\" src=\"%s\" /></a>" % (paperinfo["image"], paperinfo["image_width_percent"], paperinfo["image"])

		links_html = "<div class=\"clearright\"></div>"
		linksdict = paperinfo["local_resources"]
		if len(linksdict) > 0 :	# files not empty
			links_html = "<div class=\"files\">Local Resources: "

			for f in linksdict:
				links_html += "<a onclick=\"cc('%s')\" href=\"javascript:void(0);\">%s</a>" % (linksdict[f], f) 

			links_html += "</div>"
	
		# this block is displayed, if commentaries are showing
		sc = ""
		if show_commentaries == True :
			sc = " style=\"display: block\"" 

		extra_html ="""			
			<div class="extra" id="e%d"%s>
				<div class="commentary">
					<div class="updated">Updated: %s</div>
					<h3><a onclick="clc(%d)" href=\"javascript:void(0);\">&#9650; Commentary</a></h3>
					%s
					%s
					%s
					%s
				</div>
			</div>""" % (paper_id, sc, paperinfo["updated"], paper_id, novelty_html, image_html, md2html(paperinfo["commentary_md"]), links_html)
	
	
	# display paper

	# if a paper has commentary, it has a grey highlighted background
	wc = "details"
	if paperinfo["commentary_exists"] :
		wc = "details_wc"

	out = """
		<a name="p%d"></a>
		<div class="paper">
			<div class="%s">
				<div class="menu">
					<a href="paper?id=%d">&#9776;</a>
				</div>			
				
				<a id="pp%d" class="noSelect" onclick="pc(%d)" oncontextmenu="javascript:rpc(%d);return false;" href="javascript:void(0);">%s<span class="topic" style="color: %s; background-color: %s" id="t%d">%s</span>%s<span class="title" id="n%d">%s</span></a>
			</div>
			%s
		</div>	
	""" % (paper_id, wc, paper_id, paper_id, paper_id, paper_id, stars_html, text_colour, topic_colour, paper_id, topic, subtopic_html, paper_id, papername, extra_html)

	return out


def footerRC(num_commentaries) :

	out = """
	</div>
	<div id="footer">
		<div id="back_to_top"><a href="#">Back to top &#8679;</a></div>
	</div>
	<script type="text/javascript">
		if (document.getElementById("num_commentaries")) {
			document.getElementById("num_commentaries").innerHTML = "<strong>%d</strong> commentaries"
		}
	</script>
	""" % (num_commentaries)
	
	return out




def footerHTML() :

	out = """
</body>
</html>
	"""

	return out



def nopdf_msg1(pdf_path) :

	out = """
	<h2>ResearchCandy could not display folder</h2>
	<h2><span class="code">%s</span></h2>
	<h2>because it does not exist.</h2>
	""" % (pdf_path)

	return out



def nopdf_msg2(pdf_path) :

	out = """
	<h2>ResearchCandy could not display folder</h2>
	<h2><span class="code">%s</span></h2>
	<h2>because it does not have the correct structure.</h2>
	""" % (pdf_path)

	return out






