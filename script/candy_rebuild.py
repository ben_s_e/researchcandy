#!/usr/bin/env python3

"""Core of ResearchCandy: rebuilds the ResearchCandy index, making an up-to-date index.html, 
and hash tag list, for the pdf collection


Ben Shirt-Ediss, 2018-2019
"""


import sys
sys.path.append("..")
import settings
import candy_html
import candy_misc

import os
import pickle




VERSION = "ResearchCandy v1.0"


#
#
# File system operations 
#
#

def list_subfolders(path):
	"""
	Takes <path>, and returns in alphabetical order all sub-folders 1 level deep in path, 
	that do not start with .
	"""
	sub = []

	try :
		for s in os.listdir(path) :
			if os.path.isdir(os.path.join(path, s)) :
				if s[0] != "." :
					sub.append(s)
	
	except PermissionError:
		pass   # dirs up to the first one inaccessible are listed

	return sorted(sub)



def list_pdfs(path) :
	"""
	Returns an alphabetically ordered list of PDF files in the folder <path>.
	The .pdf file extensions are NOT included
	"""

	pdf = []

	try :	
		for f in os.listdir(path) :
			if f.endswith(".pdf") or f.endswith(".PDF") :
				pdf.append(f[:-4])	
		
	except PermissionError:
		pass 	# pdfs up to the first one inaccessible are listed

	return sorted(pdf)



def contains_pdfs(path) :
	"""
	Returns true if folder <path> contains at least 1 pdf file -- a quick check
	"""

	try :
		for f in os.listdir(path) :
			if f.endswith(".pdf") or f.endswith(".PDF") :
				return True
		
	except PermissionError:
		pass 	# pdfs up to the first one inaccessible are listed

	return False



def valid_topic(path) :
	"""
	Returns true if <path> is a valid 1st-level topic folder
	Valid 1st-level topic folders contain PDF, and/or they have daughter folders (1 level deep) which contain PDF
	"""

	if contains_pdfs(path) : 
		return True
	else :
		subtopics = list_subfolders(path)

		for s in subtopics :
			if contains_pdfs(path + "/" + s) :
				return True

		return False




#
#
# Rebuild static index page
#
# When <show_commentaries> is True, index is rebuilt with commentaries displayed
#
#

def rebuild(show_commentaries = False) :
	"""
	Returns True if rebuild succeeded and index.html page is created.
	Returns False and an error message if (a) the pdf_path to the collection does not exist or (b) the path does exist, but the folder is not formatted correctly
	"""

	# delete current index file, if it exists
	if os.path.isfile("templates/index.html") :
		os.system("rm -rf templates/index.html")
	
	# ==============================================================================
	# (Part 0) check settings.py is the correct format
	# ==============================================================================

	if hasattr(settings, 'collection_name') and hasattr(settings, 'pdf_path') and hasattr(settings, 'google_scholar') :
		# if collection title and path are empty, this is the first run and the welcome screen is displayed
		if settings.collection_name.strip() == "" and settings.pdf_path.strip() == "" :
			return -3, "welcome to ResearchCandy"

		# if just ONE of collection title or path are empty, report bad settings
		if settings.collection_name.strip() == "" or settings.pdf_path.strip() == "" :
			return -1, "settings.py is bad"

		# if path ends with backslash, report bad settings
		if settings.pdf_path[-1] == "/" :
			return -1, "settings.py is bad" 	
	else :
		return -1, "settings.py is bad" 
	

	# ==============================================================================
	# (Part 1) Read PDF names in collection into a big list, 
	#	count number of papers and topics, and generate a random colour for each topic
	# ==============================================================================

	num_topics = 0
	num_papers = 0
	bigindex = [] 		# the entire pdf collection: an alphabetically ordered list of lists (each sublist corresponds to 1 paper: either topic,subtopic,pdf_name or topic,pdf_name)
	topics = []			# list of alphabetically ordered topics
	ctopics = []		# random colour assigned to each topic

	# check pdf folder exists
	if os.path.isdir(settings.pdf_path) :

		# loop through all topic folders
		for t in list_subfolders(settings.pdf_path) :

			topic_path = settings.pdf_path + "/" + t

			# if topic is valid, display it
			if valid_topic(topic_path) :

				num_topics += 1

				topics.append(t)
				ctopics.append(candy_misc.random_topic_bg_colour())

				# (a) First, record subtopics of topic (the ones that contain PDF)
				subtopics = list_subfolders(topic_path)

				for s in subtopics :

					subtopic_path = topic_path + "/" + s

					if contains_pdfs(subtopic_path) :

						pdf = list_pdfs(subtopic_path)
						for p in pdf :
							num_papers += 1
							# topic->subtopic->pdf
							bigindex.append([t,s,p])

				# (b) Second, record PDFs in the topic
				pdf = list_pdfs(topic_path)

				for p in pdf :
					num_papers += 1
					# topic->pdf
					bigindex.append([t,p])


	# error messages
	# pdf path does not exist
	if not(os.path.isdir(settings.pdf_path)) :
		return -2, candy_html.nopdf_msg1(settings.pdf_path)

	# no pdfs in folder
	if len(bigindex) == 0 :
		return -2, candy_html.nopdf_msg2(settings.pdf_path)



	# ==============================================================================
	# (Part 2) Add metadata to PDFs, make HTML GUI page for collection, 
	#	and re-make a fresh list of all hashtags in the collection
	# ==============================================================================

	indexfile = open("templates/index.html","w")

	# set of all hashtags in the system
	filename_hashtags = "static/metadata/hashtags.pkl"
	global_hashtags = set()

	# main papers DB: paper name maps to paper attributes
	filename_papersDB = "static/metadata/papers.pkl"
	papersDB = {}
		
	# linker DB:  paper ID in list maps to paper name. This is needed so a paper can be grabbed by ID, when being updated
	filename_linkerDB = "static/metadata/linker.pkl"
	linkerDB = {}

	# if papers.pkl database already exists, read it
	if os.path.isfile(filename_papersDB) : 
		f = open(filename_papersDB, "rb")
		papersDB = pickle.load(f)
		f.close()
	
	num_commentaries = 0

	# write page headers
	indexfile.write(candy_html.headerHTML(settings.collection_name, settings.pdf_path))
	indexfile.write(candy_html.headerRC(VERSION, settings.pdf_path, show_commentaries, num_papers, num_topics, settings.collection_name))	

	# write list of papers
	for i, paperloc in enumerate(bigindex) :

		# is paper in a topic, or sub-topic?
		if len(paperloc) == 2 :
			# paper is in a topic
			t,p = paperloc 		# topic, papertitle
			s = ""				# subtopic
		else :
			# paper is in a sub-topic
			t,s,p = paperloc

		# link paper title to its list position (i.e. its paper ID for the current list)
		linkerDB[i] = p

		# get details for paper
		if papersDB.get(p, -1) == -1 :  # index paper by its title
			# paper is not in paper index, add empty details for it
			
			paperinfo = {}
			paperinfo["updated"] = ""
			paperinfo["stars"] = 0
			paperinfo["pdfname"] = p
			paperinfo["commentary_exists"] = False
			paperinfo["novelty_md"] = ""
			paperinfo["commentary_md"] = ""
			paperinfo["image"] = ""
			paperinfo["image_width_percent"] = 0
			paperinfo["image_position"] = 0
			paperinfo["image_width_px"] = 0
			paperinfo["image_height_px"] = 0			
			paperinfo["local_resources"] = {}
			paperinfo["hashtags"] = set()

			papersDB[p] = paperinfo
		else :
			# paper already in paper index, get its details
			paperinfo = papersDB[p]

			# if paper has hashtags defined, put them into the global set of hashtags
			if len(paperinfo["hashtags"]) > 0 :
				global_hashtags = global_hashtags.union(paperinfo["hashtags"])

			# count 1 more commentary
			if(paperinfo["commentary_exists"] == True) :
				num_commentaries += 1					

		# write HTML for paper
		topic_colour = ctopics[topics.index(t)]
		text_colour = candy_misc.topic_text_colour(topic_colour)

		paperHTML = candy_html.paper(i, t, s, p, text_colour, topic_colour, paperinfo, show_commentaries)
		indexfile.write(paperHTML)

	# write page footer
	indexfile.write(candy_html.footerRC(num_commentaries)) # footer updates num commentaries in header, when all papers have been read
	indexfile.write(candy_html.footerHTML())
	indexfile.close()

	# save modified papers index, as pickle file
	# XXX note - papers deleted on the filesystem remain in the .pkl database
	f = open(filename_papersDB, "wb")
	pickle.dump(papersDB,f)
	f.close()

	# save linker DB
	f = open(filename_linkerDB, "wb")
	pickle.dump(linkerDB,f)
	f.close()

	# save newly re-created hashtags index, as pickle file
	f = open(filename_hashtags, "wb")
	pickle.dump(global_hashtags,f)
	f.close()

	return 1, "Index rebuilt successfully"






if __name__ == "__main__" :

	rebuild(show_commentaries = True)



