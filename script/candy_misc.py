#!/usr/bin/env python3

"""Miscellaneous supporting functions for ResearchCandy

Ben Shirt-Ediss, 2018
"""


import sys
sys.path.append("..")
import settings

import math
import random
import datetime



#
#
# Topic colouring
#
#


# use white text in preference to black, for borderline cases
WHITE_BIAS = 1.13

# list of all topic colours used in ResearchCandy -- these are chosen to be pleasant and varied colours
CLIST = [ 	"#1E152A",
			"#4E6766",
			"#5AB1BB",
			"#A5C882",
			"#F7DD72",
			"#33658A",
			"#758E4F",
			"#F6AE2D",
			"#F26419",
			"#FED766",
			"#009FB7",
			"#DEEFB7",
			"#98DFAF",
			"#5FB49C",
			"#414288",
			"#DE6B48",
			"#E5B181",
			"#C73E1D",
			"#C59849",
			"#0E79B2",
			"#BF1363",
			"#F39237",
			"#688E26",
			"#FAA613",
			"#F44708",
			"#A10702",
			"#D81159",
			"#8F2D56",
			"#218380",
			"#73D2DE",
			"#B8D8D8",
			"#7A9E9F",
			"#4F6367",
			"#FE5F55",
			"#048BA8",
			"#16DB93",
			"#EFEA5A",
			"#7699D4",
			"#9448BC",
			"#861388",
			"#E15A97",
			"#EEABC4",
			"#C799A6" ]




def euclidean_colour_distance(hex1, hex2) :
	"""
	Returns the straight line distance between two points in RGB space
	(assumes: the shorter the line, the more related are the colours)
	"""

	r1 = int(hex1[1:3], 16); r2 = int(hex2[1:3], 16)
	g1 = int(hex1[3:5], 16); g2 = int(hex2[3:5], 16)
	b1 = int(hex1[5:7], 16); b2 = int(hex2[5:7], 16)

	sumsq = (r1-r2)**2 + (g1-g2)**2 + (b1-b2)**2

	return math.sqrt(sumsq)




def topic_text_colour(hex_bgcolour) :
	"""
	Given topic background colour <hex_bgcolour>, returns the text colour white or black 
	which is maximally far away from <hex_bgcolour> in colour space, and so is most clear against the background
	"""

	whited = euclidean_colour_distance("#FFFFFF", hex_bgcolour)
	blackd = euclidean_colour_distance("#000000", hex_bgcolour)

	# a slight bias to using white text for borderline cases is added
	if blackd >= (whited * WHITE_BIAS) :
		return "#000000"
	else :
		return "#FFFFFF"




def random_topic_bg_colour() :

	return random.choice(CLIST)









#
#
# Commentary image width (based on its aspect ratio)
#
#


# W * H aspect ratios
box_sizes = 	{	1: (4.33, 1.0),  # small thin box at the top
					2: (1.6,  1.0),
					3: (1.0,  1.0),
					4: (1.0,  1.6),
					5: (1.0,  3.0)	}

box_wpercent =  {	1: 95,
					2: 50,
					3: 40,
					4: 40,
					5: 30		}




def im_aspect(im_size) :

	(iw, ih) = im_size

	if iw < ih :
		return (1, ih/iw)
	elif ih < iw :
		return (iw/ih, 1)
	else :
		return (1,1)




def calc_commentary_image_scaling_and_position(im_size) :
	"""
	Determines what percentage width to scale an image, so it best
	fits into a commentary box. Wide images go in the top position, tall and square images go in the side position
	"""

	# convert image dimensions to normalised aspect ratio
	(iw, ih) = im_aspect(im_size)

	best_area_coverage = -1
	im_dim = None
	box_selected = -1

	for b in box_sizes :

		# get current box size
		(w, h) = box_sizes[b]

		area_coverage = 0

		# (1) re-size image to width of box
		# and keep same image aspect ratio
		ih2 = (w/iw) * ih

		if ih2 <= h : # image does not flood outside the box on rescale
			# calc percent area coverage
			im_area = w * ih2
			area_coverage = im_area / (w * h)

			if area_coverage > best_area_coverage :
				best_area_coverage = area_coverage
				im_dim = (w, ih2)
				box_selected = b

		# (2) re-size image to height of box
		# and keep same image aspect ratio
		iw2 = (h/ih) * iw

		if iw2 <= w : # image does not flood outside the box on rescale
			im_area = iw2 * h
			area_coverage = im_area / (w * h)

			if area_coverage > best_area_coverage :
				best_area_coverage = area_coverage
				im_dim = (iw2, h)		
				box_selected = b	

	if box_selected == -1 : box_selected = 3

	# convert width of scaled image to % display width
	dw = (im_dim[0] / box_sizes[box_selected][0]) * box_wpercent[box_selected]

	return int(dw), box_selected
			# percentage width, what position to display image




def fit_image_into_fixed_dimensions(im_size, fixed_size) :
	"""
	Rescales an image by x or y, so that it fits into a box of given size
	Returns -1, if the image cannot fit, e.g. if one/both dimensions require to be smaller than 1px
	Returns width and height to scale image
	"""

	(iw, ih) = im_size 			# integer
	(fw, fh) = fixed_size		# integer

	if (iw <= fw) and (ih <= fh) :
		# image is already fits inside box with no overlaps
		return iw, ih


	wh_aspect_ratio = iw / ih

	# squash by width, and see if height fits in
	scaled_h = fw * (1 / wh_aspect_ratio) 

	if (scaled_h >= 1) and (scaled_h <= fh) :
		return fw, int(scaled_h)

	else :

		# otherwise, squash by height and see if width fits in
		scaled_w = fh * wh_aspect_ratio

		if (scaled_w >= 1) and (scaled_w <= fw) :
			return int(scaled_w), fh

		else :
			# otherwise, image cannot fit in without becoming invisible
			return -1, -1


	




#
#
# Parsers 
#
#

def title_authorsyear(pname) :
	# parse paper name, into title and authors/year (rem: paper name does not have .pdf in it)

	ptitle = pname
	pdetails = ""

	dot_idx = pname.find(".")

	if dot_idx == -1 :  # no dot found
		return ptitle, pdetails
	else :
		ptitle = pname[:dot_idx]
		pdetails = pname[dot_idx+1:].strip().replace(".", ",")
		return ptitle, pdetails




def parse_hashtags(text) :
	# returns a list of all hashtags encountered in the text
	
	htags = []
	for word in text.split():
		if word.startswith("#") and len(word) > 1 and word[1] != "#" :
			htags.append(word)

	return set(htags)




def google_scholar_link(ptitle) :
	# makes a link to search google scholar for the paper title

	link = "%s/scholar?q=" % (settings.google_scholar)

	if settings.google_scholar.strip() == "" :
		link = "https://scholar.google.co.uk/scholar?q="

	for word in ptitle.split() :
		link = link + "%s+" % (word)

	return link[:-1]



def parse_external_files_md(textarea) :

	ef = {}

	for s in textarea.splitlines():

		# check format of md link
		# correct number of brackets in correct order

		if (s.count("[") != 1) or (s.count("]") != 1) or (s.count("(") != 1) or (s.count(")") != 1) :
			# string invalid - does not contain required bracket - quietly fail and move to next
			continue
		else :
			a = s.index("["); b = s.index("]"); c = s.index("("); d = s.index(")")
			if not ((a < b) and (b < c) and (c < d)) :
				# string invalid - bracket order wrong - quietly fail and move to next
				continue
			else :
				link_name = s.split("[")[1].split("]")[0].strip()
				link_loc = s.split("(")[1].split(")")[0].strip()

				ef[link_name] = link_loc

	return ef




#
#
# Current date & uploaded image names based on date
#
#


def get_date() :

	now = datetime.datetime.now()
	return now.strftime("%d %b %Y")  	# e.g. "29 Aug 2019"



def timestamp_filename(extension) :

	now = datetime.datetime.now()

	return "%s.%s" % (now.strftime("%d_%b_%Y_%H%M%S"), extension)



