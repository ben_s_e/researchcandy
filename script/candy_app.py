#!/usr/bin/env python3

"""Simple Flask web app wrapper that allows ResearchCandy to run as a web application on localhost

I don't use the advanced features of Flask, I just use it in a minimal way:
i.e for starting a local server, for url routing and for retrieving GET/POST information

ResearchCandy is started on localhost by changing to the ResearchCandy application directory, then typing
$ python3 candy_app.py

Ben Shirt-Ediss, 2019
"""

import candy_misc
import candy_rebuild


from flask import Flask, request, render_template, Markup, redirect, url_for
from PIL import Image
import pickle
import os












app = Flask(__name__)

COMMENTARY_IMAGES_FOLDER = "/static/metadata/images"
app.config['UPLOAD_FOLDER'] = app.root_path + COMMENTARY_IMAGES_FOLDER







#
#
# INDEX PAGE REQUESTS
#
#

@app.route('/', methods=['GET', 'POST'])
def rcandy_indexpage():
	

	if request.method == 'GET' :
		# --------------------------------------------------------------------------------
		# GET request
		# --------------------------------------------------------------------------------

		# check if index.html already exists
		if os.path.isfile("templates/index.html") :

			# yes - display currently built index (which may be out of date)
			return render_template("index.html")	
				# note: render_template() is needed to make css and js work
				# but serves no other purpose with the index page

		else :

			# no - generate index.html for the first time
			success_code, errormsg = candy_rebuild.rebuild(show_commentaries = False)

			if success_code == 1 : 
				return render_template("index.html")
			elif success_code == -1 : 				
				return render_template("nosettings.html")
			elif success_code == -2:
				return render_template("nopdf.html", error_html = Markup(errormsg))
			elif success_code == -3:	
				return render_template("welcome.html")


	elif request.method == 'POST' and request.form.get('rebuild') :
		# --------------------------------------------------------------------------------
		# POST request
		#	-- comes from "Rebuild index" being pressed on main index page
		# --------------------------------------------------------------------------------

		sc = False
		if request.form.get('show_commentaries') :  # only present when checkbox is checked
			sc = True  

		success_code, errormsg = candy_rebuild.rebuild(show_commentaries = sc)


		if success_code == 1 : 
			return redirect(url_for("rcandy_indexpage"))
		elif success_code == -1 : 				
			return render_template("nosettings.html")
		elif success_code == -2:
			return render_template("nopdf.html", error_html = Markup(errormsg))
		elif success_code == -3:	
			return render_template("welcome.html")
		

		








#
#
# PAPER DETAILS PAGE REQUESTS
#
#

@app.route('/paper', methods=['GET', 'POST'])
def rcandy_paperpage():

	if request.method == 'GET' and request.args.get('id') :
		# --------------------------------------------------------------------------------
		# GET request
		#	-- comes from menu button being pressed next to a paper
		# --------------------------------------------------------------------------------

		id = int(request.args.get('id'))	

		filename_hashtags = "static/metadata/hashtags.pkl"
		f = open(filename_hashtags, "rb")
		global_hashtags = pickle.load(f)
		f.close()

		filename_papersDB = "static/metadata/papers.pkl"
		f = open(filename_papersDB, "rb")
		papersDB = pickle.load(f)
		f.close()
	
		filename_linkerDB = "static/metadata/linker.pkl"
		f = open(filename_linkerDB, "rb")
		linkerDB = pickle.load(f)
		f.close()			

		paperinfo = papersDB[linkerDB[id]]  # linker turns id to paper name, then paper name used to pull out paper details
											# (because papers have changing ids as more are added)

		# 0. Get TITLE, AUTHORS, LAST UPDATED
		papertitle, authorsyear = candy_misc.title_authorsyear(paperinfo["pdfname"])

		updated = ""
		if paperinfo["updated"] != "" :
			updated = "Last Updated: %s" % (paperinfo["updated"])

		# 1. Get STARS
		stars_html = "<span class=\"s0\">&#9734;&#9734;&#9734;</span>"
		selected = {0: "selected", 1: "", 2: "", 3: ""}
		if paperinfo["stars"] == 1 :
			stars_html = "<span class=\"s1\">&#9733;</span><span class=\"s0\">&#9734;&#9734;</span>"
			selected = {0: "", 1: "selected", 2: "", 3: ""}
		elif paperinfo["stars"] == 2 :
			stars_html = "<span class=\"s2\">&#9733;&#9733;</span><span class=\"s0\">&#9734;</span>"
			selected = {0: "", 1: "", 2: "selected", 3: ""}
		elif paperinfo["stars"] == 3 :
			stars_html = "<span class=\"s3\">&#9733;&#9733;&#9733;</span>"	
			selected = {0: "", 1: "", 2: "", 3: "selected"}

		# 2. Get COMMENTARY MARKDOWN
		all_commentary_markdown = ""
		if paperinfo["novelty_md"] != "" :
			all_commentary_markdown = "%s\n\n%s" % (paperinfo["novelty_md"], paperinfo["commentary_md"])

		# 3. Get IMAGE (and render it in a fixed sized box)
		keyimage_html = "<img src=\"%s\" width=95 />" % ("/static/no_image.svg")
		img_section_heading = "Set a key image for paper"
		del_checkbox_html = ""

		if paperinfo["image"] != "" :
			# an image is already set
			img_section_heading = "Change key image for paper"
			del_checkbox_html = "<span>or</span> <input type=\"checkbox\" name=\"deleteim\" /> <span>Delete current image</span>"
								
			iw = paperinfo["image_width_px"]
			ih = paperinfo["image_height_px"]

			fitted_w, fitted_h = candy_misc.fit_image_into_fixed_dimensions((iw, ih), (100, 100))

			if fitted_w != -1 :
				# i.e. image can be fitted into box without w or h going below 1 pixel
				keyimage_html = "<img src=\"%s\" width=%d height=%d />" % (paperinfo["image"], fitted_w, fitted_h)
			
		# 4. Get LOCAL FILES LINKED TO
		all_files_markdown = ""
		for ef in paperinfo["local_resources"] :
			all_files_markdown += "[%s](%s)\n" % (ef, paperinfo["local_resources"][ef])

		# 5. Get GLOBAL LIST OF ALL HASH TAGS
		hashtags_html = ""
		if len(global_hashtags) > 0 : 
			hashtags_html = "<div id=\"tags\"><p>Hash tags previously used: </p><ul>"
			cht = list(global_hashtags)
			cht.sort()	# alphabetical
			for h in cht:
				hashtags_html += "<li><span class=\"ht\">%s</p></li>" % (h)
			hashtags_html += "</ul></div>"


		return render_template("paper.html", \
			paper_id = id, \
			paper_scholar_keywords = papertitle.replace(" ", "+"), \
			paper_title = papertitle, \
			paper_authors = authorsyear, \
			paper_last_updated = updated, \
			star0_selected = selected[0], \
			star1_selected = selected[1], \
			star2_selected = selected[2], \
			star3_selected = selected[3], \
			star0123_html = Markup(stars_html), \
			image_section_heading = img_section_heading, \
			image_html = Markup(keyimage_html), \
			deleteim_checkbox_html = Markup(del_checkbox_html), \
			external_files = all_files_markdown, \
			commentary = all_commentary_markdown, \
			htags_html = Markup(hashtags_html))

		

	elif request.method == 'POST' and request.form.get('updatepaper') :

		# --------------------------------------------------------------------------------
		# POST request
		#		-- comes from "Update paper" being pressed on paper details page
		# --------------------------------------------------------------------------------

		"""
		POST VARS REMINDER

		'updatepaper', '1'
		'id', '100'							ID OF PAPER
		'sr', '0' 							STARS
		'file', ''							PAPER IMAGE UPLOAD
		'deleteim', 'on'					DELETE CURRENT IMAGE (only present if checkbox ticked)
		'ef', 'files here'					EXTRA FILES
		'com', 'this is commentary text'	COMMENTARY TEXT
		"""

		id = int(request.form.get('id'))	

		filename_papersDB = "static/metadata/papers.pkl"
		f = open(filename_papersDB, "rb")
		papersDB = pickle.load(f)
		f.close()
	
		filename_linkerDB = "static/metadata/linker.pkl"
		f = open(filename_linkerDB, "rb")
		linkerDB = pickle.load(f)
		f.close()

		paperinfo = papersDB[linkerDB[id]]

		# 1. Update STARS
		paperinfo["stars"] = int(request.form.get('sr'))	

		# 2. Update COMMENTARY MARKDOWN
		all_commentary_markdown = (str(request.form.get('com'))).strip().replace("\r\n", "\n")
		# 	tech note: new lines in text area POST data are sent as CR LF pairs \r\n
		#   these pairs are replaced with simply \n when saving the markdown
		
		commentary_textonly = all_commentary_markdown.replace("\n", "")

		if len(commentary_textonly) > 0 :
			if "\n\n" in all_commentary_markdown:
				# novelty AND commentary specified 
				paperinfo["novelty_md"], paperinfo["commentary_md"] = all_commentary_markdown.split("\n\n",1)
			else :
				# only novelty specified
				paperinfo["novelty_md"] = all_commentary_markdown
				paperinfo["commentary_md"] = ""
		else :
			# no text specified
			paperinfo["novelty_md"] = ""
			paperinfo["commentary_md"] = ""

		# 2a. Update HASHTAGS
		htags = candy_misc.parse_hashtags(all_commentary_markdown)
		paperinfo["hashtags"] = htags

		# 3. Update IMAGE UPLOADED (adapted from https://flask.palletsprojects.com/en/1.1.x/patterns/fileuploads/)
		allowed_extensions = ["png", "jpg", "jpeg", "gif"]

		# (see if user has indicated to delete current image)
		if request.form.get('deleteim') :
			paperinfo["image"] = ""
			paperinfo["image_width_percent"] = 0
			paperinfo["image_position"] = 0
			paperinfo["image_width_px"] = 0
			paperinfo["image_height_px"] = 0

			# XXX future improvement? physically delete to old image to save disk space
		else :
			if 'file' in request.files :
				file = request.files['file']

				file_extension = ""
				if "." in file.filename :
					file_extension = file.filename.rsplit(".",1)[1].lower()
				
				allowed_file = (file_extension in allowed_extensions)
				
				if allowed_file :
					filename_on_server = candy_misc.timestamp_filename(file_extension)
					abs_filepath_on_server = os.path.join(app.config['UPLOAD_FOLDER'], filename_on_server)
					file.save(abs_filepath_on_server)

					paperinfo["image"] = COMMENTARY_IMAGES_FOLDER + "/" + filename_on_server
					
					# dimensions of image determine its display orientation
					im_width = 300
					im_height = 300
					with Image.open(abs_filepath_on_server) as img :
						im_width, im_height = img.size

					paperinfo["image_width_px"] = im_width
					paperinfo["image_height_px"] = im_height

					dw, box_selected = candy_misc.calc_commentary_image_scaling_and_position((im_width, im_height))
					paperinfo["image_width_percent"] = dw
					paperinfo["image_position"] = box_selected

				else :
					pass  # fail silently if file is the wrong type
			else :
				pass  # no file uploaded -- keep existing image file (if there is one)

		# 4. Update LOCAL FILES LINKED TO
		all_files_markdown = (str(request.form.get('ef'))).strip().replace("\r\n", "\n")

		paperinfo["local_resources"] = candy_misc.parse_external_files_md(all_files_markdown)

		# 5. determine if commentary exists
		paperinfo["commentary_exists"] = True
		paperinfo["updated"] = candy_misc.get_date()
		if paperinfo["novelty_md"] == "" and paperinfo["image"] == "" and len(paperinfo["local_resources"]) == 0 :
			paperinfo["commentary_exists"] = False
			paperinfo["updated"] = ""

		# save modified database file
		papersDB[linkerDB[id]] = paperinfo

		f = open(filename_papersDB, "wb")
		pickle.dump(papersDB,f)
		f.close()

		# re-build the index page
		success_code, errormsg = candy_rebuild.rebuild(show_commentaries = False)

		if success_code == 1 :
			# now re-direct to the newly created index page, and add a hash anchor to the url to zoom
			# to the paper
			return redirect(url_for("rcandy_indexpage") + "#p" + str(id))
		elif success_code == -1 : 				
			return render_template("nosettings.html")
		elif success_code == -2:
			return render_template("nopdf.html", error_html = Markup(errormsg))
		elif success_code == -3:	
			return render_template("welcome.html")



















#
#
# START LOCAL SERVER
#
#

if __name__ == '__main__':
	try: 
		app.run(debug=True, port=5000) #run app in debug mode on port 5000
	except OSError:
		print("")
		print("ResearchCandy web server is already running in another terminal window.")
		print("")
			# "OSError: Address already in use", occurred
			# this happens when the app is launched a second time (by clicking the app)
			# but the first instance is still running. In this case, nothing is done.
			# selecting for OSError exactly is important here.


