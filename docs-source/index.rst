ResearchCandy - A Simple and Colourful Way to Organise Academic Papers on MacOS
===============================================================================

*ResearchCandy* takes a directory of PDF research papers and displays them as a colourful list. Papers in the list can be opened by clicking, and can have user commentaries attached:



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   collection
   installation
   usage
   multiple
   coffee
