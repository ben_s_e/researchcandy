#!/usr/bin/env bash

echo "Checking Python3 is installed..."

if command -v python3 >/dev/null 2>&1 
then
	echo "Python 3 is installed."
	echo "Creating Python3 virtual environment..."

	cd script
	python3 -m venv venv
	source venv/bin/activate
	pip install --upgrade pip
	pip install -r requirements.txt
	cd ..

	mv script/applescript/ResearchCandy.app ResearchCandy.app
	mv script/applescript/ResearchCandy\ PDF.app ResearchCandy\ PDF.app
	mv script/applescript/ResearchCandy\ Alias.app ResearchCandy\ Alias.app
	mv script/applescript/ResearchCandy\ Finder.app ResearchCandy\ Finder.app

	echo ""
	echo "Done. ResearchCandy has been successfully installed."
	echo ""
else 
	echo "Python 3 is not installed. Install Python3 and then re-try."
fi

